﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Threading;
using PersonifyToPortalBuzzSync.MembersServices;
using PersonifyToPortalBuzzSync.OrgUnitServices;

namespace PersonifyToPortalBuzzSync
{
    public partial class PersonifyToPortalBuzzSyncService : ServiceBase
    {
        //Production

        //Personify 7.5 Production (PPROD)
        private const string PersonifyConnectionString =
            "Data Source=10.0.200.60;Initial Catalog=PPROD;persist security info=False;user id=clubres;password=7hu9wexaGebu;max pool size=200";

        private const string ServiceDBConnectionString =
            "data source=10.0.200.30;initial catalog=PersonifyToPortalBuzzSync;persist security info=False;user id=kiwanisone_user;password=j4ke66aAamZe;max pool size=200";

        private const string MembersApiUrl = "http://members.portalbuzz.com/api/MembersServices";
        private const string OrgUnitsApiUrl = "http://members.portalbuzz.com/api/OrgUnitsServices";
        private const string APIUserName = "5bdthnw4p58lro9";
        private const string APIPassword = "1dhLpBR~E1";

        //Development

        //Personify 7.5 Development connection string (PDEV)
        //private const string PersonifyConnectionString =
        //    "Data Source=172.17.2.63;Initial Catalog=PDEV;persist security info=False;user id=clubres;password=7hu9wexaGebu;max pool size=200";

        //private const string ServiceDBConnectionString = "data source=dev.kiwanis.org;initial catalog=PersonifyToPortalBuzzSync;persist security info=False;user id=kiwanisone_user;password=j4ke66aAamZe;max pool size=200";
        //private const string MembersApiUrl = "http://portalbuzz.miraculumsoftware.net/api/MembersServices";
        //private const string OrgUnitsApiUrl = "http://portalbuzz.miraculumsoftware.net/api/OrgUnitsServices";
        //private const string APIUserName = "abcdef012345678";
        //private const string APIPassword = "1111111111";


        public PersonifyToPortalBuzzSyncService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            var tsTask = new ThreadStart(TaskLoop);
            var myTask = new Thread(tsTask);
            myTask.Start();
        }

        private static void TaskLoop()
        {
            // In this example, task will repeat in infinite loop with while(true)
            // You can add additional parameter here if you want to have an option 
            // to stop and restart the task from some external control panel
            while (true)
            {
                // First, execute scheduled task

                TimeSpan taskLength = TimeSpan.FromMinutes(30);

                try
                {
                    taskLength = SyncMemberTask();
                }
                catch (Exception e)
                {
                    var mail = new MailMessage("noreply@portalbuzz.com", "rewing@kiwanis.org")
                        {
                            Body = "Message:\n" + e.Message + "\n\nStack Trace:\n" + e.StackTrace,
                            Subject = "Sync Error"
                        };

                    var smtp = new SmtpClient("email-smtp.us-east-1.amazonaws.com", 2587)
                        {
                            Credentials = new NetworkCredential("AKIAIX5T6YOQJ2XVOQJQ",
                                                                "ApU1l5N1knBqLad5gPw87gyFRl5w1ZzIXSJJAtOPnjSB"),
                            EnableSsl = true
                        }; //25, 465 or 587

                    smtp.Send(mail);
                }

                if (taskLength < TimeSpan.FromMinutes(30))
                {
                    Thread.Sleep(TimeSpan.FromMinutes(30));
                }

            }
            //This should be an infinite loop as the service is always running :)
// ReSharper disable FunctionNeverReturns
        }
// ReSharper restore FunctionNeverReturns

        private static TimeSpan SyncMemberTask()
        {
            var startTime = DateTime.Now;

            #region setup sync report

            int newReportId;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd = new SqlCommand("insert into SyncReport (ReportDate) values (@date)", conn)
                    {
                        CommandType = CommandType.Text
                    };

                cmd.Parameters.Add("@date", SqlDbType.DateTime);
                cmd.Parameters["@date"].Value = startTime;

                cmd.ExecuteNonQuery();
            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd = new SqlCommand("select top 1 ReportId from SyncReport order by ReportId desc", conn)
                    {
                        CommandType = CommandType.Text
                    };

                newReportId = (int)cmd.ExecuteScalar();
            }

            #endregion

            #region Org Sync

            var orgUnitClient = GetOrgUnitClient();
            var orgApiKey = GetOrgAPIKey();

            #region Modified Districts

            var modifiedDistricts = new DataSet();

            DateTime lastmodifiedDistrictsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd = new SqlCommand("select top 1 modifiedDistrictsComplete from SyncReport where modifiedDistrictsComplete is not null order by ReportId desc", conn)
                    {
                        CommandType = CommandType.Text
                    };

                lastmodifiedDistrictsComplete = (DateTime)cmd.ExecuteScalar();
            }

            DateTime modifiedDistrictStartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_modifiedDistricts", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastmodifiedDistrictsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(modifiedDistricts);
            }

            var modifiedDistrictsFound = 0;
            var modifiedDistrictsAdded = 0;

            for (var i = 0; i < modifiedDistricts.Tables[0].Rows.Count; i++)
            {
                modifiedDistrictsFound++;

                var result = UpdateOrgUnit(modifiedDistricts.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                if (result == "")
                {
                    modifiedDistrictsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set modifiedDistrictsComplete = @complete,  ModifiedDistrictsFound = " +
                        modifiedDistrictsFound + ", ModifiedDistrictsUpdated = " + modifiedDistrictsAdded +
                        "  where ReportId = " + newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = modifiedDistrictStartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region New Districts

            var newDistricts = new DataSet();

            DateTime lastNewDistrictsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewDistrictsComplete from SyncReport where NewDistrictsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewDistrictsComplete = (DateTime)cmd.ExecuteScalar();
            }

            var newDistrictStartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_NewDistricts", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewDistrictsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newDistricts);
            }

            var newDistrictsFound = 0;
            var newDistrictsAdded = 0;

            for (var i = 0; i < newDistricts.Tables[0].Rows.Count; i++)
            {
                newDistrictsFound++;

                string result = AddNewOrgUnit(newDistricts.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                if (result == "")
                {
                    newDistrictsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewDistrictsComplete = @complete,  NewDistrictsFound = " +
                        newDistrictsFound + ", NewDistrictsAdded = " + newDistrictsAdded + " where ReportId = " +
                        newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newDistrictStartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region Modified Divisions

            var modifiedDivisions = new DataSet();

            DateTime lastmodifiedDivisionsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 modifiedDivisionsComplete from SyncReport where modifiedDivisionsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastmodifiedDivisionsComplete = (DateTime)cmd.ExecuteScalar();
            }

            DateTime modifiedDivisionstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_modifiedDivisions", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastmodifiedDivisionsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(modifiedDivisions);
            }

            var modifiedDivisionsFound = 0;
            var modifiedDivisionsAdded = 0;

            for (var i = 0; i < modifiedDivisions.Tables[0].Rows.Count; i++)
            {
                modifiedDivisionsFound++;

                var result = UpdateOrgUnit(modifiedDivisions.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                if (result == "")
                {
                    modifiedDivisionsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set modifiedDivisionsComplete = @complete,  ModifiedDivisionsFound = " +
                        modifiedDivisionsFound + ", ModifiedDivisionsUpdated = " + modifiedDivisionsAdded +
                        "  where ReportId = " + newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = modifiedDivisionstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region New Divisions

            var newDivisions = new DataSet();

            DateTime lastNewDivisionsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewDivisionsComplete from SyncReport where NewDivisionsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewDivisionsComplete = (DateTime)cmd.ExecuteScalar();
            }

            var newDivisionstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_NewDivisions", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewDivisionsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newDivisions);
            }

            var newDivisionsFound = 0;
            var newDivisionsAdded = 0;

            for (int i = 0; i < newDivisions.Tables[0].Rows.Count; i++)
            {
                newDivisionsFound++;

                string result = AddNewOrgUnit(newDivisions.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                if (result == "")
                {
                    newDivisionsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewDivisionsComplete = @complete,  NewDivisionsFound = " +
                        newDivisionsFound + ", NewDivisionsAdded = " + newDivisionsAdded + "  where ReportId = " +
                        newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newDivisionstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region Modified Clubs

            var modifiedClubs = new DataSet();

            DateTime lastmodifiedClubsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 modifiedClubsComplete from SyncReport where modifiedClubsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastmodifiedClubsComplete = (DateTime)cmd.ExecuteScalar();
            }

            var modifiedClubstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_modifiedClubs", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastmodifiedClubsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(modifiedClubs);
            }

            var modifiedClubsFound = 0;
            var modifiedClubsAdded = 0;

            for (var i = 0; i < modifiedClubs.Tables[0].Rows.Count; i++)
            {
                modifiedClubsFound++;

                try
                {
                    var result = UpdateOrgUnit(modifiedClubs.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                    if (result == "")
                    {
                        modifiedClubsAdded++;
                    }
                    else
                    {
                        LogError(result, newReportId);
                    }
                }
                catch (Exception e)
                {
                    LogError(e.ToString(), newReportId);
                }
                

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set modifiedClubsComplete = @complete,  ModifiedClubsFound = " +
                        modifiedClubsFound + ", ModifiedClubsUpdated = " + modifiedClubsAdded + "  where ReportId = " +
                        newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = modifiedClubstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region New Clubs

            var newClubs = new DataSet();

            DateTime lastNewClubsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewClubsComplete from SyncReport where NewClubsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewClubsComplete = (DateTime)cmd.ExecuteScalar();
            }

            DateTime newClubstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBOrgSync_NewClubs", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewClubsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newClubs);
            }

            var newClubsFound = 0;
            var newClubsAdded = 0;

            for (var i = 0; i < newClubs.Tables[0].Rows.Count; i++)
            {
                newClubsFound++;

                var result = AddNewOrgUnit(newClubs.Tables[0].Rows[i], orgUnitClient, orgApiKey);

                if (result == "")
                {
                    newClubsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewClubsComplete = @complete,  NewClubsFound = " + newClubsFound +
                        ", NewClubsAdded = " + newClubsAdded + " where ReportId = " + newReportId, conn)
                        {
                            CommandType = CommandType.Text
                        };

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newClubstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion


            #endregion

            #region Members Sync

            var membersClient = GetMembersServiceClient();
            var memberApiKey = GetMemberAPIKey();

            #region Modified Members

            try
            {

                var modifiedMembers = new DataSet();

                DateTime lastModifiedMembersComplete;

                using (var conn = new SqlConnection(ServiceDBConnectionString))
                {
                    conn.Open();

                    var cmd =
                        new SqlCommand(
                            "select top 1 ModifiedMembersComplete from SyncReport where ModifiedMembersComplete is not null order by ReportId desc",
                            conn) {CommandType = CommandType.Text};

                    lastModifiedMembersComplete = (DateTime) cmd.ExecuteScalar();
                }

                var modifiedMemberstartTime = DateTime.Now;

                using (var conn = new SqlConnection(PersonifyConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand("kiwone_PBMemberSync_ModifiedMembers", conn);
                    cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                    cmd.Parameters["@lastRun"].Value = lastModifiedMembersComplete;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 300;
                    var adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(modifiedMembers);
                }

                var modifiedMembersFound = modifiedMembers.Tables[0].Rows.Count;
                var modifiedMembersAdded = 0;

                for (var i = 0; i < modifiedMembersFound; i++)
                {
                    var result = UpdateMember(modifiedMembers.Tables[0].Rows[i], membersClient, memberApiKey);

                    if (string.IsNullOrEmpty(result))
                    {
                        modifiedMembersAdded++;
                    }
                    else
                    {
                        LogError(result, newReportId);
                    }

                }

                using (var conn = new SqlConnection(ServiceDBConnectionString))
                {
                    conn.Open();

                    var cmd =
                        new SqlCommand(
                            "update SyncReport set ModifiedMembersComplete = @complete,  ModifiedMembersFound = " +
                            modifiedMembersFound + ", ModifiedMembersUpdated = " + modifiedMembersAdded +
                            " where ReportId = " + newReportId, conn) {CommandType = CommandType.Text};

                    cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                    cmd.Parameters["@complete"].Value = modifiedMemberstartTime;

                    cmd.ExecuteNonQuery();
                }

            }
            catch (Exception e)
            {
                var mail = new MailMessage("noreply@portalbuzz.com", "rewing@kiwanis.org")
                    {
                        Body = "Message:\n" + e.Message + "\n\nStack Trace:\n" + e.StackTrace,
                        Subject = "Sync Error - Modified Member"
                    };

                var smtp = new SmtpClient("email-smtp.us-east-1.amazonaws.com", 2587)
                    {
                        Credentials = new NetworkCredential("AKIAIX5T6YOQJ2XVOQJQ",
                                                            "ApU1l5N1knBqLad5gPw87gyFRl5w1ZzIXSJJAtOPnjSB"),
                        EnableSsl = true
                    }; //25, 465 or 587

                smtp.Send(mail);
            }

            #endregion

            #region New Members

            var newMembers = new DataSet();

            DateTime lastNewMembersComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewMembersComplete from SyncReport where NewMembersComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewMembersComplete = (DateTime)cmd.ExecuteScalar();
            }

            var newMemberstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBMemberSync_NewMembers", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewMembersComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newMembers);
            }

            var newMembersFound = newMembers.Tables[0].Rows.Count;
            var newMembersAdded = 0;

            for (var i = 0; i < newMembersFound; i++)
            {
                var result = AddNewMember(newMembers.Tables[0].Rows[i], membersClient, memberApiKey);

                if (string.IsNullOrEmpty(result))
                {
                    newMembersAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewMembersComplete = @complete,  NewMembersFound = " + newMembersFound +
                        ", NewMembersAdded = " + newMembersAdded + " where ReportId = " + newReportId, conn)
                        {
                            CommandType = CommandType.Text
                        };

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newMemberstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region Modified Memberships

            var modifiedMemberships = new DataSet();

            DateTime lastModifiedMembershipsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 ModifiedMembershipsComplete from SyncReport where ModifiedMembershipsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastModifiedMembershipsComplete = (DateTime)cmd.ExecuteScalar();
            }

            var modifiedMembershipstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBMemberSync_ModifiedMemberships", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastModifiedMembershipsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(modifiedMemberships);
            }

            var modifiedMembershipsFound = modifiedMemberships.Tables[0].Rows.Count;
            var modifiedMembershipsAdded = 0;

            for (var i = 0; i < modifiedMembershipsFound; i++)
            {
                var result = UpdateMembership(modifiedMemberships.Tables[0].Rows[i], membersClient, memberApiKey, orgUnitClient, orgApiKey);

                if (string.IsNullOrEmpty(result))
                {
                    modifiedMembershipsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set ModifiedMembershipsComplete = @complete,  ModifiedMembershipsFound = " +
                        modifiedMembershipsFound + ", ModifiedMembershipsUpdated = " + modifiedMembershipsAdded +
                        " where ReportId = " + newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = modifiedMembershipstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region New Memberships

            var newMemberships = new DataSet();

            DateTime lastNewMembershipsComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewMembershipsComplete from SyncReport where NewMembershipsComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewMembershipsComplete = (DateTime)cmd.ExecuteScalar();
            }

            var newMembershipstartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBMemberSync_NewMemberships", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewMembershipsComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newMemberships);
            }

            var newMembershipsFound = newMemberships.Tables[0].Rows.Count;
            var newMembershipsAdded = 0;

            for (var i = 0; i < newMembershipsFound; i++)
            {
                var result = AddNewMembership(newMemberships.Tables[0].Rows[i], membersClient, memberApiKey, orgUnitClient, orgApiKey);

                if (string.IsNullOrEmpty(result))
                {
                    newMembershipsAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewMembershipsComplete = @complete,  NewMembershipsFound = " +
                        newMembershipsFound + ", NewMembershipsAdded = " + newMembershipsAdded + " where ReportId = " +
                        newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newMembershipstartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region Modified Roles

            var modifiedRoles = new DataSet();

            DateTime lastModifiedRolesComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 ModifiedRolesComplete from SyncReport where ModifiedRolesComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastModifiedRolesComplete = (DateTime)cmd.ExecuteScalar();
            }

            var modifiedRolestartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBMemberSync_ModifiedRoles", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastModifiedRolesComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(modifiedRoles);
            }

            var modifiedRolesFound = modifiedRoles.Tables[0].Rows.Count;
            var modifiedRolesAdded = 0;

            for (var i = 0; i < modifiedRolesFound; i++)
            {
                var result = UpdateRoles(modifiedRoles.Tables[0].Rows[i], membersClient, memberApiKey, orgUnitClient, orgApiKey);

                if (string.IsNullOrEmpty(result))
                {
                    modifiedRolesAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set ModifiedRolesComplete = @complete,  ModifiedRolesFound = " +
                        modifiedRolesFound + ", ModifiedRolesUpdated = " + modifiedRolesAdded + " where ReportId = " +
                        newReportId, conn) {CommandType = CommandType.Text};

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = modifiedRolestartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion

            #region New Roles

            var newRoles = new DataSet();

            DateTime lastNewRolesComplete;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "select top 1 NewRolesComplete from SyncReport where NewRolesComplete is not null order by ReportId desc",
                        conn) {CommandType = CommandType.Text};

                lastNewRolesComplete = (DateTime)cmd.ExecuteScalar();
            }

            var newRolestartTime = DateTime.Now;

            using (var conn = new SqlConnection(PersonifyConnectionString))
            {
                conn.Open();
                var cmd = new SqlCommand("kiwone_PBMemberSync_NewRoles", conn);
                cmd.Parameters.Add("@lastRun", SqlDbType.DateTime);
                cmd.Parameters["@lastRun"].Value = lastNewRolesComplete;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandTimeout = 60;
                var adapter = new SqlDataAdapter(cmd);
                adapter.Fill(newRoles);
            }

            var newRolesFound = newRoles.Tables[0].Rows.Count;
            var newRolesAdded = 0;

            for (var i = 0; i < newRolesFound; i++)
            {
                var result = AddNewRoles(newRoles.Tables[0].Rows[i], membersClient, memberApiKey, orgUnitClient,
                                            orgApiKey);

                if (string.IsNullOrEmpty(result))
                {
                    newRolesAdded++;
                }
                else
                {
                    LogError(result, newReportId);
                }

            }

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "update SyncReport set NewRolesComplete = @complete,  NewRolesFound = " + newRolesFound +
                        ", NewRolesAdded = " + newRolesAdded + " where ReportId = " + newReportId, conn)
                        {
                            CommandType = CommandType.Text
                        };

                cmd.Parameters.Add("@complete", SqlDbType.DateTime);
                cmd.Parameters["@complete"].Value = newRolestartTime;

                cmd.ExecuteNonQuery();
            }

            #endregion


            #endregion

            var endTime = DateTime.Now;

            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand("update SyncReport set ElapsedTime = @elapsedTime where reportId = " + newReportId,
                                   conn);

                cmd.Parameters.Add("@elapsedTime", SqlDbType.NVarChar);
                cmd.Parameters["@elapsedTime"].Value = (endTime - startTime).ToString();

                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();
            }

            orgUnitClient.Close();
            membersClient.Close();

            return (endTime - startTime);
        }

        protected override void OnStop()
        {
        }

        private static void LogError(string error, int reportId)
        {
            using (var conn = new SqlConnection(ServiceDBConnectionString))
            {
                conn.Open();

                var cmd =
                    new SqlCommand(
                        "insert into Errors (ReportId, Message, ErrorDate) values (" + reportId + ", '" +
                        error.Replace("'", "''") + "', getdate())", conn) {CommandType = CommandType.Text};

                cmd.ExecuteNonQuery();
            }
        }






        private static string AddMemberAndRelationship(MembersServiceClient membersClient,
                                               MembersServices.ApiKey memberApiKey,
                                               string memberMasterCustomerId, string clubMasterCustomerID,
                                               DateTime begin)
        {
            var error = "";
            var singleMember = new DataSet();
            try
            {
                using (var conn = new SqlConnection(PersonifyConnectionString))
                {


                    conn.Open();
                    var cmd = new SqlCommand("kiwone_PBSync_singleUser", conn);
                    cmd.Parameters.Add("@masterCustomerId", SqlDbType.VarChar);
                    cmd.Parameters["@masterCustomerId"].Value = memberMasterCustomerId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 60;
                    var adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(singleMember);
                }

                if (singleMember.Tables[0].Rows.Count > 0)
                {

                    var result = membersClient.AddWithoutOrgUnit(
                        memberApiKey,
                        singleMember.Tables[0].Rows[0]["FIRST_NAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["LAST_NAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["NICKNAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["NAME_PREFIX"].ToString(),
                        singleMember.Tables[0].Rows[0]["NAME_SUFFIX"].ToString(),
                        singleMember.Tables[0].Rows[0]["PRIMARY_EMAIL_ADDRESS"].ToString(),
                        singleMember.Tables[0].Rows[0]["MASTER_CUSTOMER_ID"].ToString());
                    if (result.Ok)
                    {
                        if (singleMember.Tables[0].Rows[0]["related_master_customer_id"].ToString() !=
                            clubMasterCustomerID)
                        {
                            var subResult = membersClient.AddRelationship(
                                memberApiKey,
                                memberMasterCustomerId,
                                clubMasterCustomerID,
                                begin,
                                null
                                );
                            if (!subResult.Ok)
                            {
                                error = subResult.ErrorMessage +
                                        " | Retry relationship add, added member, add relationship failed: member:" +
                                        memberMasterCustomerId + " club:" + clubMasterCustomerID + " beginDate:" +
                                        begin;
                            }
                        }
                    }
                    else
                    {
                        error = result.ErrorMessage + " | Retry relationship add, add member first failed: member:" +
                                singleMember.Tables[0].Rows[0]["MASTER_CUSTOMER_ID"] + " club:" +
                                singleMember.Tables[0].Rows[0]["related_master_customer_id"] + " fistName:" +
                                singleMember.Tables[0].Rows[0]["FIRST_NAME"] + " lastName:" +
                                singleMember.Tables[0].Rows[0]["LAST_NAME"] + " nickName:" +
                                singleMember.Tables[0].Rows[0]["NICKNAME"] + " prefix:" +
                                singleMember.Tables[0].Rows[0]["NAME_PREFIX"] + " suffix:" +
                                singleMember.Tables[0].Rows[0]["NAME_SUFFIX"] + " email:" +
                                singleMember.Tables[0].Rows[0]["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                                ((DateTime)singleMember.Tables[0].Rows[0]["begin_date"]);
                    }

                }

            }
            catch (Exception e)
            {
                error = "Error Message: " + e.Message + "Stack Track: " + e.StackTrace;
            }

            return error;
        }

        private static string AddNewMember(DataRow dr, MembersServiceClient membersClient, MembersServices.ApiKey memberApiKey)
        {
            string error = "";

            MemberSyncResult result = membersClient.AddWithoutOrgUnit(
                memberApiKey,
                dr["FIRST_NAME"].ToString(),
                dr["LAST_NAME"].ToString(),
                dr["NICKNAME"].ToString(),
                dr["NAME_PREFIX"].ToString(),
                dr["NAME_SUFFIX"].ToString(),
                dr["PRIMARY_EMAIL_ADDRESS"].ToString(),
                dr["MASTER_CUSTOMER_ID"].ToString());

            if (!result.Ok)
            {
                #region Handle Add Error

                switch (result.ErrorMessage)
                {
                    case "There is user with specified e-mail already":
                        {
                            MemberSyncResult subResult = membersClient.AddWithoutOrgUnit(
                                memberApiKey,
                                dr["FIRST_NAME"].ToString(),
                                dr["LAST_NAME"].ToString(),
                                dr["NICKNAME"].ToString(),
                                dr["NAME_PREFIX"].ToString(),
                                dr["NAME_SUFFIX"].ToString(),
                                null,
                                dr["MASTER_CUSTOMER_ID"].ToString());
                            if (!subResult.Ok)
                            {
                                error = result.ErrorMessage + " | Retry Member Add with null email failed: member:" +
                                        dr["MASTER_CUSTOMER_ID"] + " club:" +
                                        dr["related_master_customer_id"] + " email:" +
                                        dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                                        dr["begin_date"];
                            }
                        }
                        break;
                    //case "There is no organization with such ID":
                    //    {
                    //        string subFunctionResult = AddSingleOrg(orgUnitClient, orgApiKey,
                    //                                                dr["related_master_customer_id"].ToString());

                    //        if (string.IsNullOrEmpty(subFunctionResult))
                    //        {
                    //            MemberSyncResult subResult = membersClient.AddWithoutOrgUnit(
                    //                memberApiKey,
                    //                dr["FIRST_NAME"].ToString(),
                    //                dr["LAST_NAME"].ToString(),
                    //                dr["NICKNAME"].ToString(),
                    //                dr["NAME_PREFIX"].ToString(),
                    //                dr["NAME_SUFFIX"].ToString(),
                    //                null,
                    //                dr["MASTER_CUSTOMER_ID"].ToString());
                    //            if (!subResult.Ok)
                    //            {
                    //                error = subResult.ErrorMessage +
                    //                        " | Retry Member Add, adding org first, retry member failed: member:" +
                    //                        dr["MASTER_CUSTOMER_ID"] + " club:" +
                    //                        dr["related_master_customer_id"] + " email:" +
                    //                        dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                    //                        dr["begin_date"];
                    //            }
                    //        }
                    //        else
                    //        {
                    //            error = subFunctionResult + " | Retry Member Add, adding org first failed: member:" +
                    //                    dr["MASTER_CUSTOMER_ID"] + " club:" +
                    //                    dr["related_master_customer_id"] + " email:" +
                    //                    dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                    //                    dr["begin_date"];
                    //        }
                    //    }
                    //    break;
                    case "There is already user with this ID":
                        MemberSyncResult subResult2 = membersClient.UpdateWithoutOrgUnit(
                            memberApiKey,
                            dr["FIRST_NAME"].ToString(),
                            dr["LAST_NAME"].ToString(),
                            dr["NICKNAME"].ToString(),
                            dr["NAME_PREFIX"].ToString(),
                            dr["NAME_SUFFIX"].ToString(),
                            dr["PRIMARY_EMAIL_ADDRESS"].ToString(),
                            dr["MASTER_CUSTOMER_ID"].ToString(),
                            false,
                            "");

                        if (!subResult2.Ok)
                        {
                            error = result.ErrorMessage + "; Retry member add as member update: " + subResult2.ErrorMessage + " | member:" +
                                        dr["MASTER_CUSTOMER_ID"] + " club:" +
                                        dr["related_master_customer_id"] + " email:" +
                                        dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                                        dr["begin_date"];
                        }
                        break;
                    default:
                        error = result.ErrorMessage + " | Member Add: member:" + dr["MASTER_CUSTOMER_ID"] +
                                " club:" + dr["related_master_customer_id"] + " email:" +
                                dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" + dr["begin_date"];
                        break;
                }

                #endregion
            }

            return error;
        }

        private static string AddNewMembership(DataRow dr, MembersServiceClient membersClient,
                                       MembersServices.ApiKey memberApiKey, OrgUnitsServiceClient orgUnitClient,
                                   OrgUnitServices.ApiKey orgApiKey)
        {
            string error = "";

            DateTime begin = DateTime.Today;
            if (dr["begin_date"] != DBNull.Value)
            {
                begin = (DateTime)dr["begin_date"];
            }
            if (begin > DateTime.Today)
            {
                begin = DateTime.Today;
            }

            MemberSyncResult result = membersClient.AddRelationship(
                memberApiKey,
                dr["MASTER_CUSTOMER_ID"].ToString(),
                dr["related_master_customer_id"].ToString(),
                begin,
                null
                );

            if (!result.Ok)
            {
                switch (result.ErrorMessage)
                {
                    case "There is no user with such ID":
                        {
                            string subFunctionResult = AddMemberAndRelationship(membersClient,
                                                                                memberApiKey,
                                                                                dr["MASTER_CUSTOMER_ID"].ToString(),
                                                                                dr["related_master_customer_id"].ToString(),
                                                                                begin);
                            if (subFunctionResult != "")
                            {
                                error = subFunctionResult;
                            }
                        }
                        break;
                    case "There is no organization with such ID":
                        error = AddSingleOrg(orgUnitClient, orgApiKey, dr["Related_master_customer_id"].ToString());
                        if (string.IsNullOrEmpty(error))
                        {
                            MemberSyncResult subResult = membersClient.AddRelationship(
                                memberApiKey,
                                dr["MASTER_CUSTOMER_ID"].ToString(),
                                dr["related_master_customer_id"].ToString(),
                                begin,
                                null
                                );
                            if (!subResult.Ok)
                            {
                                error += subResult.ErrorMessage + " | retry add relationship, adding org first, add relationship failed: member:" + dr["MASTER_CUSTOMER_ID"] +
                                         " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                            }
                        }
                        else
                        {
                            error += " | retry add relationship, adding org first, add org failed: member:" + dr["MASTER_CUSTOMER_ID"] +
                                     " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                        }
                        break;
                    default:
                        error = result.ErrorMessage + " | Add Relationship: member:" + dr["MASTER_CUSTOMER_ID"] +
                                " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                        break;
                }
            }

            return error;
        }

        private static string AddNewRoles(DataRow dr, MembersServiceClient membersClient,
                                  MembersServices.ApiKey memberApiKey, OrgUnitsServiceClient orgUnitClient,
                                  OrgUnitServices.ApiKey orgApiKey)
        {
            string error = "";

            MemberSyncResult result = membersClient.AssignRole(memberApiKey,
                                                               dr["Related_master_customer_id"].ToString(),
                                                               dr["Master_customer_id"].ToString(),
                                                               GetRole(
                                                                   dr["USR_MEMBERSHIP_CLASS_CODE"].ToString().Substring(
                                                                       0, 2), dr["relationship_code"].ToString()));

            if (!result.Ok)
            {
                switch (result.ErrorMessage)
                {
                    case "User is not a member of the organizatiom":
                    {
                        var beginDate = dr["begin_date"] != System.DBNull.Value
                            ? (DateTime) dr["begin_date"]
                            : DateTime.Today;

                        if (beginDate > DateTime.Today)
                        {
                            beginDate = DateTime.Today;
                        }

                        var endDate = dr["end_date"] != DBNull.Value ? (DateTime?) dr["end_date"] : null;

                            MemberSyncResult subResult = membersClient.AddRelationship(memberApiKey,
                                                                                       dr["Master_customer_id"].ToString
                                                                                           (),
                                                                                       dr["Related_master_customer_id"].
                                                                                           ToString(), beginDate,
                                                                                       endDate);

                            if (subResult.Ok)
                            {
                                MemberSyncResult subSubResult = membersClient.AssignRole(memberApiKey,
                                                                                         dr["Related_master_customer_id"
                                                                                             ].ToString(),
                                                                                         dr["Master_customer_id"].
                                                                                             ToString(),
                                                                                         GetRole(
                                                                                             dr[
                                                                                                 "USR_MEMBERSHIP_CLASS_CODE"
                                                                                                 ].ToString().Substring(
                                                                                                     0, 2),
                                                                                             dr["relationship_code"].
                                                                                                 ToString()));

                                if (!subSubResult.Ok)
                                {
                                    error = subSubResult.ErrorMessage +
                                            " | Retry Assign Rold Adding membership first, membership added, retry assign failed: member:" +
                                            dr["Master_customer_id"] + " club:" +
                                            dr["Related_master_customer_id"] + " role:" +
                                            dr["relationship_code"];
                                }
                            }
                            else
                            {
                                error = subResult.ErrorMessage +
                                        " | Retry Assign Role adding membership first, membership add failed: member:" +
                                        dr["Master_customer_id"] + " club:" +
                                        dr["Related_master_customer_id"] + " role:" +
                                        dr["relationship_code"];
                            }
                        }
                        break;
                    case "There is no organization with such ID":

                        error = AddSingleOrg(orgUnitClient, orgApiKey, dr["Related_master_customer_id"].ToString());

                        if (string.IsNullOrEmpty(error))
                        {
                            MemberSyncResult subResult = membersClient.AssignRole(memberApiKey,
                                                                                  dr["Related_master_customer_id"].
                                                                                      ToString(),
                                                                                  dr["Master_customer_id"].ToString(),
                                                                                  GetRole(
                                                                                      dr["USR_MEMBERSHIP_CLASS_CODE"].
                                                                                          ToString().Substring(0, 2),
                                                                                      dr["relationship_code"].ToString()));
                            if (!subResult.Ok)
                            {
                                error = subResult.ErrorMessage +
                                        " | Retry Assign Role adding org first, org added, retry assign role failed: member:" +
                                        dr["Master_customer_id"] + " club:" +
                                        dr["Related_master_customer_id"] + " role:" +
                                        dr["relationship_code"];
                            }
                        }
                        else
                        {
                            error += " | Retry Assign Role adding org first, add org failed: member:" +
                                     dr["Master_customer_id"] + " club:" +
                                     dr["Related_master_customer_id"] + " role:" +
                                     dr["relationship_code"];
                        }

                        break;
                    case "There is no user with specified external user ID":

                        error = AddSingleMember(membersClient, memberApiKey, dr["Master_customer_id"].ToString());

                        if (string.IsNullOrEmpty(error))
                        {
                            MemberSyncResult subResult = membersClient.AssignRole(memberApiKey,
                                                                                  dr["Related_master_customer_id"].
                                                                                      ToString(),
                                                                                  dr["Master_customer_id"].ToString(),
                                                                                  GetRole(
                                                                                      dr["USR_MEMBERSHIP_CLASS_CODE"].
                                                                                          ToString().Substring(0, 2),
                                                                                      dr["relationship_code"].ToString()));
                            if (!subResult.Ok)
                            {
                                error = subResult.ErrorMessage +
                                        " | Retry Assign Role adding member first, member added, retry assign role failed: member:" +
                                        dr["Master_customer_id"] + " club:" +
                                        dr["Related_master_customer_id"] + " role:" +
                                        dr["relationship_code"];
                            }
                        }
                        else
                        {
                            error += " | Retry Assign Role adding member first, add member failed: member:" +
                                     dr["Master_customer_id"] + " club:" +
                                     dr["Related_master_customer_id"] + " role:" +
                                     dr["relationship_code"];
                        }

                        break;
                    default:
                        error = result.ErrorMessage + " | Assign Role: member:" + dr["Master_customer_id"] +
                                " club:" + dr["Related_master_customer_id"] + " role:" +
                                dr["relationship_code"];
                        break;
                }
            }

            return error;
        }

        private static string AddNewOrgUnit(DataRow dr, IOrgUnitsService orgUnitClient,
                                            OrgUnitServices.ApiKey orgApiKey)
        {
            var error = "";

            DateTime? start;
            DateTime? end;

            if (dr["USR_ORG_JOIN_DATE"] == DBNull.Value)
                start = DateTime.Today;
            else
                start = (DateTime)dr["USR_ORG_JOIN_DATE"];

            if (start > DateTime.Today)
            {
                start = DateTime.Today;
            }

            if (dr["endDate"] == DBNull.Value)
                end = null;
            else
                end = (DateTime)dr["endDate"];

            var result = orgUnitClient.Add(orgApiKey,
                                                         GetType(dr["USR_MEMBERSHIP_CLASS_CODE"].ToString(),
                                                                 dr["MASTER_CUSTOMER_ID"].ToString()),
                                                         dr["MASTER_CUSTOMER_ID"].ToString(),
                                                         dr["LABEL_NAME"].ToString(),
                                                         dr["RELATED_MASTER_CUSTOMER_ID"].ToString(),
                                                         start,
                                                         end,
                                                         dr["Country"].ToString(),
                                                         dr["State"].ToString().ToUpper());


            if (!result.Ok)
            {
                if (result.ErrorMessage == "There is already organization unit with specified external ID")
                {
                    OrgUnitSyncResult subResult = orgUnitClient.Update(orgApiKey,
                                                                dr["MASTER_CUSTOMER_ID"].ToString(),
                                                                dr["LABEL_NAME"].ToString(),
                                                                dr["RELATED_MASTER_CUSTOMER_ID"].ToString(),
                                                                start,
                                                                end,
                                                                dr["Country"].ToString(),
                                                                dr["State"].ToString().ToUpper());
                    if (!subResult.Ok)
                    {
                        error = result.ErrorMessage + "; Retry Org Update: " + subResult.ErrorMessage + " | Org Add: club:" + dr["MASTER_CUSTOMER_ID"] + " parent:" +
                            dr["RELATED_MASTER_CUSTOMER_ID"] + " country:" + dr["Country"] + " state:" +
                            dr["State"].ToString().ToUpper();
                    }
                }
                else
                {
                    error = result.ErrorMessage + " | Org Add: club:" + dr["MASTER_CUSTOMER_ID"] + " parent:" +
                            dr["RELATED_MASTER_CUSTOMER_ID"] + " country:" + dr["Country"] + " state:" +
                            dr["State"].ToString().ToUpper();
                }
            }

            return error;
        }

        private static string AddSingleMember(IMembersService membersClient, MembersServices.ApiKey memberApiKey,
                                      string memberMasterCustomerId)
        {
            var error = "";
            var singleMember = new DataSet();
            try
            {
                using (var conn = new SqlConnection(PersonifyConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand("kiwone_PBSync_singleUser", conn);
                    cmd.Parameters.Add("@masterCustomerId", SqlDbType.VarChar);
                    cmd.Parameters["@masterCustomerId"].Value = memberMasterCustomerId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    var adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(singleMember);
                }

                if (singleMember.Tables[0].Rows.Count > 0)
                {

                    var result = membersClient.AddWithoutOrgUnit(
                        memberApiKey,
                        singleMember.Tables[0].Rows[0]["FIRST_NAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["LAST_NAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["NICKNAME"].ToString(),
                        singleMember.Tables[0].Rows[0]["NAME_PREFIX"].ToString(),
                        singleMember.Tables[0].Rows[0]["NAME_SUFFIX"].ToString(),
                        singleMember.Tables[0].Rows[0]["PRIMARY_EMAIL_ADDRESS"].ToString(),
                        singleMember.Tables[0].Rows[0]["MASTER_CUSTOMER_ID"].ToString());
                    if (!result.Ok)
                    {
                        error = result.ErrorMessage;
                    }
                }
            }
            catch (Exception e)
            {
                error = "Error Message: " + e.Message + "Stack Track: " + e.StackTrace;
            }

            return error;
        }

        private static string AddSingleOrg(IOrgUnitsService orgClient, OrgUnitServices.ApiKey orgApiKey,
                                   string clubMasterCustomerId)
        {
            string error = "";

            var singleOrg = new DataSet();
            try
            {
                using (var conn = new SqlConnection(PersonifyConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand("PBOrgSync_singleOrg", conn);
                    cmd.Parameters.Add("@clubMasterCustomerId", SqlDbType.VarChar);
                    cmd.Parameters["@clubMasterCustomerId"].Value = clubMasterCustomerId;
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 60;
                    var adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(singleOrg);
                }

                if (singleOrg.Tables[0].Rows.Count > 0)
                {
                    DateTime? endDate;

                    if (singleOrg.Tables[0].Rows[0]["endDate"] == DBNull.Value)
                    {
                        endDate = null;
                    }
                    else
                    {
                        endDate = (DateTime)singleOrg.Tables[0].Rows[0]["endDate"];
                    }

                    DateTime? startDate;
                    if (singleOrg.Tables[0].Rows[0]["USR_ORG_JOIN_DATE"] == DBNull.Value)
                    {
                        startDate = DateTime.Now;
                    }
                    else
                    {
                        startDate = (DateTime)singleOrg.Tables[0].Rows[0]["USR_ORG_JOIN_DATE"];
                    }


                    var result = orgClient.Add(
                        orgApiKey,
                        GetType(singleOrg.Tables[0].Rows[0]["USR_MEMBERSHIP_CLASS_CODE"].ToString(),
                                singleOrg.Tables[0].Rows[0]["MASTER_CUSTOMER_ID"].ToString()),
                        singleOrg.Tables[0].Rows[0]["MASTER_CUSTOMER_ID"].ToString(),
                        singleOrg.Tables[0].Rows[0]["Label_name"].ToString(),
                        singleOrg.Tables[0].Rows[0]["related_master_customer_id"].ToString(),
                        startDate,
                        endDate,
                        singleOrg.Tables[0].Rows[0]["Country"].ToString(),
                        singleOrg.Tables[0].Rows[0]["State"].ToString().ToUpper());


                    if (!result.Ok)
                    {
                        error = result.ErrorMessage;
                    }
                }

            }
            catch (Exception e)
            {
                error = "Error Message: " + e.Message + "Stack Track: " + e.StackTrace;
            }

            return error;
        }

        private static string GetRole(string status, string psfyRole)
        {
            var role = "";
            switch (status)
            {
                case "AK":
                    switch (psfyRole)
                    {
                        case "FACADV":
                            role = "AC Faculty Advisor";
                            break;
                        case "COFACADV":
                            role = "AC Co-Faculty Advisor";
                            break;
                        case "KIWADV":
                            role = "AC Faculty Advisor"; //only Aktion club sets kiwanis advisors as faculty advisors
                            break;
                        case "SEC":
                            role = "AC Club Secretary";
                            break;
                        case "SECYASST":
                            role = "AC Club Secretary";
                            break;
                        case "PRES":
                            role = "AC Club President";
                            break;
                        case "PREELCT":
                            role = "AC Club President-elect";
                            break;
                        case "TREAS":
                            role = "AC Club Treasurer";
                            break;
                        case "VP":
                            role = "AC Club Vice President";
                            break;
                        case "DISTADMN":
                            role = "AC Advisor";
                            break;
                        case "AKADM":
                            role = "AC Advisor";
                            break;
                        case "BCADM":
                            role = "AC Advisor";
                            break;
                        case "CKADM":
                            role = "AC Advisor";
                            break;
                        case "KCADM":
                            role = "AC Advisor";
                            break;
                        case "KKADM":
                            role = "AC Advisor";
                            break;
                    }
                    break;
                case "KC":
                    switch (psfyRole)
                    {
                        case "FACADV":
                            role = "KCI Faculty Advisor";
                            break;
                        case "COFACADV":
                            role = "KCI Co-Faculty Advisor";
                            break;
                        case "KIWADV":
                            role = "KCI Advisor";
                            break;
                        case "SEC":
                            role = "KCI Club Secretary";
                            break;
                        case "SECYASST":
                            role = "KCI Club Secretary";
                            break;
                        case "PRES":
                            role = "KCI Club President";
                            break;
                        case "PREELCT":
                            role = "KCI Club President-elect";
                            break;
                        case "TREAS":
                            role = "KCI Club Treasurer";
                            break;
                        case "VP":
                            role = "KCI Club Vice President";
                            break;
                        case "DISTADMN":
                            role = "KCI Advisor";
                            break;
                        case "AKADM":
                            role = "KCI Advisor";
                            break;
                        case "BCADM":
                            role = "KCI Advisor";
                            break;
                        case "CKADM":
                            role = "KCI Advisor";
                            break;
                        case "KCADM":
                            role = "KCI Advisor";
                            break;
                        case "KKADM":
                            role = "KCI Advisor";
                            break;
                    }
                    break;
                case "CK":
                    switch (psfyRole)
                    {
                        case "FACADV":
                            role = "CKI Faculty Advisor";
                            break;
                        case "COFACADV":
                            role = "CKI Co-Faculty Advisor";
                            break;
                        case "KIWADV":
                            role = "CKI Advisor";
                            break;
                        case "SEC":
                            role = "CKI Club Secretary";
                            break;
                        case "SECYASST":
                            role = "CKI Club Secretary";
                            break;
                        case "PRES":
                            role = "CKI Club President";
                            break;
                        case "PREELCT":
                            role = "CKI Club President-elect";
                            break;
                        case "TREAS":
                            role = "CKI Club Treasurer";
                            break;
                        case "VP":
                            role = "CKI Club Vice President";
                            break;
                        case "DISTADMN":
                            role = "CKI Advisor";
                            break;
                        case "AKADM":
                            role = "CKI Advisor";
                            break;
                        case "BCADM":
                            role = "CKI Advisor";
                            break;
                        case "CKADM":
                            role = "CKI Advisor";
                            break;
                        case "KCADM":
                            role = "CKI Advisor";
                            break;
                        case "KKADM":
                            role = "CKI Advisor";
                            break;
                    }
                    break;
                default:
                    switch (psfyRole)
                    {
                        case "FACADV":
                            role = "KI Faculty Advisor";
                            break;
                        case "COFACADV":
                            role = "KI Co-Faculty Advisor";
                            break;
                        case "KIWADV":
                            role = "KI SLP Advisor";
                            break;
                        case "SEC":
                            role = "KI Club Secretary";
                            break;
                        case "SECYASST":
                            role = "KI Club Secretary";
                            break;
                        case "PRES":
                            role = "KI Club President";
                            break;
                        case "PREELCT":
                            role = "KI Club President-elect";
                            break;
                        case "TREAS":
                            role = "KI Club Treasurer";
                            break;
                        case "CLUBGRW":
                            role = "KI Club Membership Chair";
                            break;
                        case "VP":
                            role = "KI Club Vice President";
                            break;
                        case "KIEFBoard":
                            role = "KIEF Board";
                            break;
                        case "NonNorthAmericanDistSec":
                            role = "KI District Secretary (non-NA)";
                            break;
                        case "DISTSECY":
                            role = "KI District Secretary";
                            break;
                        case "DISTADMSEC":
                            role = "KI District Secretary";
                            break;
                        case "KIDISTASST":
                            role = "KI District Secretary (non-NA)";
                            break;
                        case "DISTLTGOV":
                            role = "KI Lt Governor";
                            break;
                        case "DISTGOV":
                            role = "KI District Governor";
                            break;
                        case "DISTIPGOV":
                            role = "KI District Governor";
                            break;
                        case "DISTGOVEL":
                            role = "KI District Governor";
                            break;
                        case "DISTVIGOV":
                            role = "KI District Governor";
                            break;
                        case "DISTADMN":
                            role = "KI SLP Advisor";
                            break;
                        case "DISTAKTN":
                            role = "AC District Admin";
                            break;
                        case "BCADM":
                            role = "BC District Admin";
                            break;
                        case "CKADM":
                            role = "CKI District Admin";
                            break;
                        case "CKASTADM":
                            role = "CKI District Admin";
                            break;
                        case "KCADM":
                            role = "KC District Admin";
                            break;
                        case "KCASTADM":
                            role = "KC District Admin";
                            break;
                        case "KKADM":
                            role = "KK District Admin";
                            break;
                        case "CERTINST":
                            role = "KI Certified Instructor";
                            break;
                        case "MASTERINST":
                            role = "KI Master Instructor";
                            break;
                        case "BYLAWASST":
                            role = "KI Bylaws Asst";
                            break;
                        case "DISTRPTVIEW":
                            role = "KI District Report Viewer";
                            break;

                        //Growth Campaign Roles
                        case "GRWDISCHR1":
                            role = "District Chair Region 1";
                            break;
                        case "GRWDISCHR2":
                            role = "District Chair Region 2";
                            break;
                        case "GRWDISCHR3":
                            role = "District Chair Region 3";
                            break;
                        case "GRWDISCHR4":
                            role = "District Chair Region 4";
                            break;
                        case "GRWDISCHRCAN":
                            role = "Growth District Chair-Canada-Caribbean";
                            break;
                        case "GRWDISCHRLA":
                            role = "Growth District Chair-Latin America";
                            break;
                        case "GRWDISVCHR1":
                            role = "District Vice-Chair Region 1";
                            break;

                        case "GRWDISVCHR2":
                            role = "District Vice-Chair Region 2";
                            break;

                        case "GRWDISVCHR3":
                            role = "District Vice-Chair Region 3";
                            break;

                        case "GRWDISVCHR4":
                            role = "District Vice-Chair Region 4";
                            break;

                        case "GRWDISVCHRAP":
                            role = "District Vice-Chair Asia-Pacific";
                            break;

                        case "GRWDISVCHRCA":
                            role = "District Vice-Chair Canada-Caribbean";
                            break;

                        case "GRWDISVCHREU":
                            role = "District Vice-Chair Europe";
                            break;

                        case "GRWDISVCHRLA":
                            role = "District Vice-Chair Latin America";
                            break;
                        case "GRWDISTASPAC":
                            role = "Growth Chairs-Asia Pacific";
                            break;
                        case "GRWDISCHRAP":
                            role = "Growth Chairs-Asia Pacific";
                            break;
                        case "GRWDISCHREU":
                            role = "Growth District Chair Europe";
                            break;
                        case "GrowthCampaignInternational":
                            role = "International Growth Admin";
                            break;

                        case "GrowthCampaignRegional":
                            role = "Regional Growth Admin";
                            break;

                        case "GRWDIVNCB":
                            role = "New Club Builder";
                            break;

                        case "GRWDIVCC":
                            role = "Club Counselor";
                            break;

                        case "GrowthCampaignClub":
                            role = "Club Growth Admin";
                            break;

                        case "InternationalPresident":
                            role = "International President";
                            break;

                        case "ExecutiveDirector":
                            role = "Executive Director";
                            break;


                        //Kiwanis Staff Roles
                        case "STAFFLOCATOR":
                            role = "Kiwanis Staff - Locator Admin";
                            break;
                        case "STAFFNCBADMN":
                            role = "Kiwanis Staff - NCB Admin";
                            break;
                        case "STAFFMBRSRCH":
                            role = "Kiwanis Staff - Club Member Search";
                            break;
                        case "STAFFSUPRADM":
                            role = "Kiwanis Staff - Super Admin";
                            break;
                        case "STAFFBYLAW":
                            role = "Kiwanis Staff - Bylaws";
                            break;
                        case "STAFFRDS":
                            role = "Kiwanis Staff - Regional Development Strategists";
                            break;
                        case "STAFFGRWADMN":
                            role = "Kiwanis Staff - Growth Admin";
                            break;
                           
                    }
                    break;
            }

            return role;
        }

        private static int GetType(string membershipClassCode, string masterCustomerId)
        {
            int type;

            switch (membershipClassCode.Substring(0, 2))
            {
                case "AK":
                    switch (membershipClassCode.Substring(2))
                    {
                        case "DIST":
                            type = 6;
                            break;
                        default: //CLUB
                            type = 7;
                            break;
                    }
                    break;
                case "CK":
                    switch (membershipClassCode.Substring(2))
                    {
                        case "DIST":
                            type = 6;
                            break;
                        default: //CLUB
                            type = 7;
                            break;
                    }
                    break;
                case "KC":
                    switch (membershipClassCode.Substring(2))
                    {
                        case "DIST":
                            type = 6;
                            break;
                        case "DIV":
                            type = 8;
                            break;
                        default: //CLUB
                            string parentCode;
                            try
                            {
                                using (var conn = new SqlConnection(PersonifyConnectionString))
                                {
                                    conn.Open();
                                    var cmd = new SqlCommand("kiwone_PBOrgSync_GetParentMembershipClassCode", conn);
                                    cmd.Parameters.Add("@clubMasterCustomerId", SqlDbType.VarChar);
                                    cmd.Parameters["@clubMasterCustomerId"].Value = masterCustomerId;
                                    cmd.CommandType = CommandType.StoredProcedure;
                                    cmd.CommandTimeout = 60;
                                    parentCode = (string)cmd.ExecuteScalar();
                                }
                            }
                            catch (Exception e)
                            {
                                var mail = new MailMessage("noreply@portalbuzz.com", "rewing@kiwanis.org")
                                    {
                                        Body =
                                            "Tried to find out if parent was KCDIV or KCDIST for master customer id: " +
                                            masterCustomerId + "\nDefault of KCDIST was given\n\n\nMessage:\n" + e.Message +
                                            "\n\nStack Trace:\n" + e.StackTrace,
                                        Subject = "Handled Sync Error"
                                    };

                                var smtp = new SmtpClient("email-smtp.us-east-1.amazonaws.com", 2587)
                                    {
                                        Credentials = new NetworkCredential("AKIAIX5T6YOQJ2XVOQJQ",
                                                                            "ApU1l5N1knBqLad5gPw87gyFRl5w1ZzIXSJJAtOPnjSB"),
                                        EnableSsl = true
                                    }; //25, 465 or 587

                                smtp.Send(mail);

                                parentCode = "KCDIST";
                            }

                            if (parentCode == null)
                            {
                                parentCode = "KCDIST";

                                var mail = new MailMessage("noreply@portalbuzz.com", "rewing@kiwanis.org")
                                    {
                                        Body =
                                            "Tried to find out if parent was KCDIV or KCDIST for master customer id: " +
                                            masterCustomerId + "\nDefault of KCDIST was given",
                                        Subject = "KC parent problem"
                                    };

                                var smtp = new SmtpClient("email-smtp.us-east-1.amazonaws.com", 2587)
                                    {
                                        Credentials = new NetworkCredential("AKIAIX5T6YOQJ2XVOQJQ",
                                                                            "ApU1l5N1knBqLad5gPw87gyFRl5w1ZzIXSJJAtOPnjSB"),
                                        EnableSsl = true
                                    }; //25, 465 or 587

                                smtp.Send(mail);
                            }

                            switch (parentCode.Substring(2))
                            {
                                case "DIV":
                                    type = 9;
                                    break;
                                default:
                                    type = 7;
                                    break;
                            }
                            break;
                    }
                    break;
                default: //KI
                    if (membershipClassCode.ToUpper() == "KI")
                    {
                        type = 1;
                    }
                    else
                    {
                        switch (membershipClassCode.Substring(2))
                        {
                            case "DIST":
                                type = 3;
                                break;
                            case "DIV":
                                type = 4;
                                break;
                            default: //CLUB
                                type = masterCustomerId.ToUpper() == "K-STAFF" ? 19 : 5;
                                break;
                        } 
                    }
                    
                    break;
            }

            return type;
        }

        private static string UpdateMember(DataRow dr, MembersServiceClient membersClient,
                                   MembersServices.ApiKey memberApiKey)
        {
            string error = "";

            if (dr["CUSTOMER_STATUS_CODE"].ToString().ToUpper() == "DUPL")
            {
                var dupMember = new DataSet();
                using (var conn = new SqlConnection(PersonifyConnectionString))
                {
                    conn.Open();
                    var cmd = new SqlCommand("select distinct RELATED_MASTER_CUSTOMER_ID from CUS_RELATIONSHIP where master_customer_id = '" + dr["MASTER_CUSTOMER_ID"] + "' and RELATIONSHIP_TYPE = 'EMPLOYMENT' and RECIPROCAL_CODE = 'EMPLOYER'", conn);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 60;
                    var adapter = new SqlDataAdapter(cmd);
                    adapter.Fill(dupMember);
                }

                foreach (DataRow row in dupMember.Tables[0].Rows)
                {
                    var result = membersClient.Delete(
                    memberApiKey,
                    dr["MASTER_CUSTOMER_ID"].ToString(),
                    row["related_master_customer_id"].ToString(),
                    DateTime.Today
                    );

                    if (!result.Ok)
                    {
                        error += result.ErrorMessage;
                    }
                }
                
            }
            else
            {
                MemberSyncResult result = membersClient.UpdateWithoutOrgUnit(
                    memberApiKey,
                    dr["FIRST_NAME"].ToString(),
                    dr["LAST_NAME"].ToString(),
                    dr["NICKNAME"].ToString(),
                    dr["NAME_PREFIX"].ToString(),
                    dr["NAME_SUFFIX"].ToString(),
                    dr["PRIMARY_EMAIL_ADDRESS"].ToString(),
                    dr["MASTER_CUSTOMER_ID"].ToString(),
                    false,
                    "");

                if (!result.Ok)
                {
                    #region Handle error

                    switch (result.ErrorMessage)
                    {
                        //Commented out because UpdateWithoutOrgUnit should no longer look at organization
                        //case "User does not belong to specified organization units":
                        //    {
                        //        string subFunctionResult = UpdateRelationshipAndMember(membersClient,
                        //                                                               memberApiKey,
                        //                                                               dr["MASTER_CUSTOMER_ID"].ToString(),
                        //                                                               dr["related_master_customer_id"].
                        //                                                                   ToString(),
                        //                                                               (DateTime)dr["begin_date"],
                        //                                                               dr["FIRST_NAME"].ToString(),
                        //                                                               dr["LAST_NAME"].ToString(),
                        //                                                               dr["NICKNAME"].ToString(),
                        //                                                               dr["NAME_PREFIX"].ToString(),
                        //                                                               dr["NAME_SUFFIX"].ToString(),
                        //                                                               dr["PRIMARY_EMAIL_ADDRESS"].ToString());

                        //        if (!string.IsNullOrEmpty(subFunctionResult))
                        //        {
                        //            error = subFunctionResult;
                        //        }
                        //    }
                        //    break;
                        //case "User does not belong to any of associated organization units":
                        //    {
                        //        string subFunctionResult = UpdateRelationshipAndMember(membersClient,
                        //                                                               memberApiKey,
                        //                                                               dr["MASTER_CUSTOMER_ID"].ToString(),
                        //                                                               dr["related_master_customer_id"].
                        //                                                                   ToString(),
                        //                                                               (DateTime)dr["begin_date"],
                        //                                                               dr["FIRST_NAME"].ToString(),
                        //                                                               dr["LAST_NAME"].ToString(),
                        //                                                               dr["NICKNAME"].ToString(),
                        //                                                               dr["NAME_PREFIX"].ToString(),
                        //                                                               dr["NAME_SUFFIX"].ToString(),
                        //                                                               dr["PRIMARY_EMAIL_ADDRESS"].ToString());

                        //        if (!string.IsNullOrEmpty(subFunctionResult))
                        //        {
                        //            error = subFunctionResult;
                        //        }
                        //    }
                        //    break;
                        case "There is user with specified e-mail already":
                            {
                                MemberSyncResult subResult = membersClient.UpdateWithoutOrgUnit(
                                    memberApiKey,
                                    dr["FIRST_NAME"].ToString(),
                                    dr["LAST_NAME"].ToString(),
                                    dr["NICKNAME"].ToString(),
                                    dr["NAME_PREFIX"].ToString(),
                                    dr["NAME_SUFFIX"].ToString(),
                                    null,
                                    dr["MASTER_CUSTOMER_ID"].ToString(),
                                    false,
                                    "");
                                if (!subResult.Ok)
                                {
                                    error = subResult.ErrorMessage + " | Retry Member Modify with null email: member:" +
                                            dr["MASTER_CUSTOMER_ID"] + " club:" +
                                            dr["related_master_customer_id"] + " email:" +
                                            dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                                            dr["begin_date"];
                                }
                            }
                            break;
                        //case "There is no organization with such ID":
                        //    {
                        //        var subFunctionResult = AddSingleOrg(orgUnitClient, orgApiKey,
                        //                                                dr["related_master_customer_id"].ToString());

                        //        if (string.IsNullOrEmpty(subFunctionResult))
                        //        {
                        //            var subResult = membersClient.AddWithoutOrgUnit(
                        //                memberApiKey,
                        //                dr["FIRST_NAME"].ToString(),
                        //                dr["LAST_NAME"].ToString(),
                        //                dr["NICKNAME"].ToString(),
                        //                dr["NAME_PREFIX"].ToString(),
                        //                dr["NAME_SUFFIX"].ToString(),
                        //                null,
                        //                dr["MASTER_CUSTOMER_ID"].ToString());
                        //            if (!subResult.Ok)
                        //            {
                        //                error = subResult.ErrorMessage +
                        //                        " | Retry Member Add, adding org first, retry member failed: member:" +
                        //                        dr["MASTER_CUSTOMER_ID"] + " club:" +
                        //                        dr["related_master_customer_id"] + " email:" +
                        //                        dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                        //                        dr["begin_date"];
                        //            }
                        //        }
                        //        else
                        //        {
                        //            error = subFunctionResult + " | Retry Member Update, adding org first failed: member:" +
                        //                    dr["MASTER_CUSTOMER_ID"] + " club:" +
                        //                    dr["related_master_customer_id"] + " email:" +
                        //                    dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                        //                    dr["begin_date"];
                        //        }
                        //    }
                        //    break;
                        case "There is no user with such ID":
                            {
                                var subResult = membersClient.AddWithoutOrgUnit(
                                    memberApiKey,
                                    dr["FIRST_NAME"].ToString(),
                                    dr["LAST_NAME"].ToString(),
                                    dr["NICKNAME"].ToString(),
                                    dr["NAME_PREFIX"].ToString(),
                                    dr["NAME_SUFFIX"].ToString(),
                                    dr["PRIMARY_EMAIL_ADDRESS"].ToString(),
                                    dr["MASTER_CUSTOMER_ID"].ToString());
                                if (!subResult.Ok)
                                {
                                    error = subResult.ErrorMessage +
                                            " | Retry Member Update as Add, Add member failed: member:" +
                                            dr["MASTER_CUSTOMER_ID"] + " club:" +
                                            dr["related_master_customer_id"] + " email:" +
                                            dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" +
                                            dr["begin_date"];
                                }
                            }
                            break;
                        default:
                            error = result.ErrorMessage + " | Member Modify: member:" + dr["MASTER_CUSTOMER_ID"] +
                                    " club:" + dr["related_master_customer_id"] + " email:" +
                                    dr["PRIMARY_EMAIL_ADDRESS"] + " beginDate:" + dr["begin_date"];
                            break;
                    }

                    #endregion
                }
            }
                

            return error;
        }

        private static string UpdateMembership(DataRow dr, MembersServiceClient membersClient,
                                       MembersServices.ApiKey memberApiKey, IOrgUnitsService orgUnitClient,
                                   OrgUnitServices.ApiKey orgApiKey)
        {
            var error = "";

            if (dr["end_date"] != DBNull.Value && (DateTime)dr["end_date"] < DateTime.Now)
            {
                var result = membersClient.Delete(
                    memberApiKey,
                    dr["MASTER_CUSTOMER_ID"].ToString(),
                    dr["related_master_customer_id"].ToString(),
                    (DateTime)dr["end_date"]
                    );

                if (!result.Ok)
                {
                    error = result.ErrorMessage;
                }
            }
            else
            {
                var begin = DateTime.Today;
                if (dr["begin_date"] != DBNull.Value)
                {
                    begin = (DateTime)dr["begin_date"];
                }
                if (begin > DateTime.Today)
                {
                    begin = DateTime.Today;
                }

                var result = membersClient.AddRelationship(
                    memberApiKey,
                    dr["MASTER_CUSTOMER_ID"].ToString(),
                    dr["related_master_customer_id"].ToString(),
                    begin,
                    null
                    );

                if (!result.Ok)
                {
                    switch (result.ErrorMessage)
                    {
                        case "There is no user with such ID":
                            {
                                var subFunctionResult = AddMemberAndRelationship(membersClient,
                                                                                    memberApiKey,
                                                                                    dr["MASTER_CUSTOMER_ID"].ToString(),
                                                                                    dr["related_master_customer_id"].ToString(),
                                                                                    begin);
                                if (subFunctionResult != "")
                                {
                                    error = subFunctionResult;
                                }
                            }
                            break;
                        case "There is no organization with such ID":
                            error = AddSingleOrg(orgUnitClient, orgApiKey, dr["Related_master_customer_id"].ToString());
                            if (string.IsNullOrEmpty(error))
                            {
                                var subResult = membersClient.AddRelationship(
                                    memberApiKey,
                                    dr["MASTER_CUSTOMER_ID"].ToString(),
                                    dr["related_master_customer_id"].ToString(),
                                    begin,
                                    null
                                    );
                                if (!subResult.Ok)
                                {
                                    error += subResult.ErrorMessage + " | retry add relationship, adding org first, add relationship failed: member:" + dr["MASTER_CUSTOMER_ID"] +
                                             " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                                }
                            }
                            else
                            {
                                error += " | retry add relationship, adding org first, add org failed: member:" + dr["MASTER_CUSTOMER_ID"] +
                                         " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                            }
                            break;
                        default:
                            error = result.ErrorMessage + " | Add Relationship: member:" + dr["MASTER_CUSTOMER_ID"] +
                                    " club:" + dr["related_master_customer_id"] + " beginDate:" + begin;
                            break;
                    }
                }
            }

            return error;
        }

        private static string UpdateOrgUnit(DataRow dr, IOrgUnitsService orgUnitClient,
                                            OrgUnitServices.ApiKey orgApiKey)
        {
            var error = "";

            DateTime? start;
            DateTime? end;

            if (dr["USR_ORG_JOIN_DATE"] == DBNull.Value)
                start = null;
            else
                start = (DateTime)dr["USR_ORG_JOIN_DATE"];

            if (start > DateTime.Today)
            {
                start = DateTime.Today;
            }

            if (dr["endDate"] == DBNull.Value)
                end = null;
            else
                end = (DateTime)dr["endDate"];

            if (end.HasValue && end.Value < DateTime.Now)
            {

                var result = orgUnitClient.Delete(orgApiKey,
                                                                dr["MASTER_CUSTOMER_ID"].ToString());

                if (!result.Ok)
                {
                    error = result.ErrorMessage + " | Org Update: club:" + dr["MASTER_CUSTOMER_ID"] +
                            " parent:" + dr["RELATED_MASTER_CUSTOMER_ID"] + " country:" +
                            dr["Country"] + " state:" + dr["State"].ToString().ToUpper();
                }
            }
            else
            {
                var result = orgUnitClient.Update(orgApiKey,
                                                                dr["MASTER_CUSTOMER_ID"].ToString(),
                                                                dr["LABEL_NAME"].ToString(),
                                                                dr["RELATED_MASTER_CUSTOMER_ID"].ToString(),
                                                                start,
                                                                end,
                                                                dr["Country"].ToString(),
                                                                dr["State"].ToString().ToUpper());

                if (!result.Ok)
                {
                    if (result.ErrorMessage == "There is no organization with such ID")
                    {
                        var subFunctionResult = AddSingleOrg(orgUnitClient, orgApiKey,
                                                                dr["RELATED_MASTER_CUSTOMER_ID"].ToString());
                        subFunctionResult += AddSingleOrg(orgUnitClient, orgApiKey, dr["MASTER_CUSTOMER_ID"].ToString());

                        if (!string.IsNullOrEmpty(subFunctionResult))
                        {
                            error = subFunctionResult;
                        }
                    }
                    else
                    {
                        error = result.ErrorMessage + " | Org Update: club:" + dr["MASTER_CUSTOMER_ID"] +
                                " parent:" + dr["RELATED_MASTER_CUSTOMER_ID"] + " country:" +
                                dr["Country"] + " state:" + dr["State"].ToString().ToUpper();
                    }
                }
            }

            return error;
        }

        //private static string UpdateRelationshipAndMember(IMembersService membersClient,
        //                                          MembersServices.ApiKey memberApiKey,
        //                                          string memberMasterCustomerId, string clubMasterCustomerID,
        //                                          DateTime begin, string firstName, string lastName,
        //                                          string nickName, string prefix, string suffix, string email)
        //{
        //    var error = "";
        //    try
        //    {

        //        var result = membersClient.AddRelationship(
        //            memberApiKey,
        //            memberMasterCustomerId,
        //            clubMasterCustomerID,
        //            begin,
        //            null
        //            );
        //        if (result.Ok)
        //        {
        //            var subResult = membersClient.UpdateWithoutOrgUnit(
        //                memberApiKey,
        //                firstName,
        //                lastName,
        //                nickName,
        //                prefix,
        //                suffix,
        //                email,
        //                memberMasterCustomerId,
        //                false,
        //                "");
        //            if (!subResult.Ok)
        //            {
        //                error = subResult.ErrorMessage +
        //                        " | Retry member update adding relationship first, relationship added, member update failed: member:" +
        //                        memberMasterCustomerId + " club:" + clubMasterCustomerID + " firstName:" + firstName +
        //                        " lastName:" + lastName + " nickName:" + nickName + " prefix:" + prefix + " suffix:" +
        //                        suffix + " email:" + email + " beginDate:" + begin;
        //            }
        //        }
        //        else
        //        {
        //            error = result.ErrorMessage +
        //                    " | Retry member update adding relationship first: member, relationship add failed:" +
        //                    memberMasterCustomerId + " club:" + clubMasterCustomerID + " beginDate:" + begin;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        error = "Error Message: " + e.Message + " StackTrack: " + e.StackTrace;
        //    }

        //    return error;
        //}

        private static string UpdateRoles(DataRow dr, MembersServiceClient membersClient,
                                          MembersServices.ApiKey memberApiKey, OrgUnitsServiceClient orgUnitClient,
                                          OrgUnitServices.ApiKey orgApiKey)
        {
            string error = "";

            if (dr["end_date"] != DBNull.Value && (DateTime)dr["end_date"] < DateTime.Now)
            {
                
                MemberSyncResult result = membersClient.RevokeRole(memberApiKey,
                                                                   dr["Related_master_customer_id"].ToString(),
                                                                   dr["Master_customer_id"].ToString(),
                                                                   GetRole(
                                                                       dr["USR_MEMBERSHIP_CLASS_CODE"].ToString().
                                                                           Substring(0, 2),
                                                                       dr["relationship_code"].ToString()));

                

                if (!result.Ok)
                {
                    error = result.ErrorMessage + " | Revoke Role: member:" + dr["Master_customer_id"] +
                            " club:" + dr["Related_master_customer_id"] + " role:" +
                            dr["relationship_code"];
                }
                else
                {
                    if (!dr["USR_MEMBERSHIP_CLASS_CODE"].ToString().ToUpper().Contains("CLUB"))
                    {
                        membersClient.Delete(memberApiKey,
                                            dr["MASTER_CUSTOMER_ID"].ToString(),
                                            dr["related_master_customer_id"].ToString(),
                                            DateTime.Now);
                    }
                }
            }
            else
            {
                MemberSyncResult result = membersClient.AssignRole(memberApiKey,
                                                                   dr["Related_master_customer_id"].ToString(),
                                                                   dr["Master_customer_id"].ToString(),
                                                                   GetRole(
                                                                       dr["USR_MEMBERSHIP_CLASS_CODE"].ToString().
                                                                           Substring(0, 2),
                                                                       dr["relationship_code"].ToString()));

                if (!result.Ok)
                {
                    switch (result.ErrorMessage)
                    {
                        case "User is not a member of the organizatiom":
                            {
                                var beginDate = dr["begin_date"] != System.DBNull.Value
                            ? (DateTime)dr["begin_date"]
                            : DateTime.Today;
                                if (beginDate > DateTime.Today)
                                {
                                    beginDate = DateTime.Today;
                                }

                                var endDate = dr["end_date"] != DBNull.Value ? (DateTime?)dr["end_date"] : null;
                                MemberSyncResult subResult = membersClient.AddRelationship(memberApiKey,
                                                                                           dr["Master_customer_id"].ToString
                                                                                               (),
                                                                                           dr["Related_master_customer_id"].
                                                                                               ToString(), beginDate,
                                                                                           endDate);

                                if (subResult.Ok)
                                {
                                    MemberSyncResult subSubResult = membersClient.AssignRole(memberApiKey,
                                                                                             dr["Related_master_customer_id"
                                                                                                 ].ToString(),
                                                                                             dr["Master_customer_id"].
                                                                                                 ToString(),
                                                                                             GetRole(
                                                                                                 dr[
                                                                                                     "USR_MEMBERSHIP_CLASS_CODE"
                                                                                                     ].ToString().Substring(
                                                                                                         0, 2),
                                                                                                 dr["relationship_code"].
                                                                                                     ToString()));

                                    if (!subSubResult.Ok)
                                    {
                                        error = subSubResult.ErrorMessage +
                                                " | Retry Assign Rold Adding membership first, membership added, retry assign failed: member:" +
                                                dr["Master_customer_id"] + " club:" +
                                                dr["Related_master_customer_id"] + " role:" +
                                                dr["relationship_code"];
                                    }
                                }
                                else
                                {
                                    error = subResult.ErrorMessage +
                                            " | Retry Assign Role adding membership first, membership add failed: member:" +
                                            dr["Master_customer_id"] + " club:" +
                                            dr["Related_master_customer_id"] + " role:" +
                                            dr["relationship_code"];
                                }
                            }
                            break;
                        case "There is no organization with such ID":

                            error = AddSingleOrg(orgUnitClient, orgApiKey, dr["Related_master_customer_id"].ToString());

                            if (string.IsNullOrEmpty(error))
                            {
                                MemberSyncResult subResult = membersClient.AssignRole(memberApiKey,
                                                                                      dr["Related_master_customer_id"].
                                                                                          ToString(),
                                                                                      dr["Master_customer_id"].ToString(),
                                                                                      GetRole(
                                                                                          dr["USR_MEMBERSHIP_CLASS_CODE"].
                                                                                              ToString().Substring(0, 2),
                                                                                          dr["relationship_code"].ToString()));
                                if (!subResult.Ok)
                                {
                                    error = subResult.ErrorMessage +
                                            " | Retry Assign Role adding org first, org added, retry assign role failed: member:" +
                                            dr["Master_customer_id"] + " club:" +
                                            dr["Related_master_customer_id"] + " role:" +
                                            dr["relationship_code"];
                                }
                            }
                            else
                            {
                                error += " | Retry Assign Role adding org first, add org failed: member:" +
                                         dr["Master_customer_id"] + " club:" +
                                         dr["Related_master_customer_id"] + " role:" +
                                         dr["relationship_code"];
                            }

                            break;
                        case "There is no user with specified external user ID":

                            error = AddSingleMember(membersClient, memberApiKey, dr["Master_customer_id"].ToString());

                            if (string.IsNullOrEmpty(error))
                            {
                                MemberSyncResult subResult = membersClient.AssignRole(memberApiKey,
                                                                                      dr["Related_master_customer_id"].
                                                                                          ToString(),
                                                                                      dr["Master_customer_id"].ToString(),
                                                                                      GetRole(
                                                                                          dr["USR_MEMBERSHIP_CLASS_CODE"].
                                                                                              ToString().Substring(0, 2),
                                                                                          dr["relationship_code"].ToString()));
                                if (!subResult.Ok)
                                {
                                    error = subResult.ErrorMessage +
                                            " | Retry Assign Role adding member first, member added, retry assign role failed: member:" +
                                            dr["Master_customer_id"] + " club:" +
                                            dr["Related_master_customer_id"] + " role:" +
                                            dr["relationship_code"];
                                }
                            }
                            else
                            {
                                error += " | Retry Assign Role adding member first, add member failed: member:" +
                                         dr["Master_customer_id"] + " club:" +
                                         dr["Related_master_customer_id"] + " role:" +
                                         dr["relationship_code"];
                            }

                            break;
                        default:
                            error = result.ErrorMessage + " | Assign Role: member:" + dr["Master_customer_id"] +
                                    " club:" + dr["Related_master_customer_id"] + " role:" +
                                    dr["relationship_code"];
                            break;
                    }
                }
            }

            return error;
        }











        private static MembersServices.ApiKey GetMemberAPIKey()
        {
            var apiKey = new MembersServices.ApiKey
                             {
                                 ClientId = APIUserName,
                                 ClientPassword = APIPassword
                             };
            return apiKey;
        }

        private static OrgUnitServices.ApiKey GetOrgAPIKey()
        {
            var apiKey = new OrgUnitServices.ApiKey
                             {
                                 ClientId = APIUserName,
                                 ClientPassword = APIPassword
                             };
            return apiKey;
        }

        private static MembersServiceClient GetMembersServiceClient()
        {
            var client = new MembersServiceClient("basicHttpBinding", MembersApiUrl);
            return client;
        }

        private static OrgUnitsServiceClient GetOrgUnitClient()
        {
            var client = new OrgUnitsServiceClient("basicHttpBinding1", OrgUnitsApiUrl);
            return client;
        }










    }
}